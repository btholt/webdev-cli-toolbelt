#source

# based on nyrpnz.com

# Generate a temporary file in /tmp
MDTEMPFILE="/tmp/mdview-"`date +%s`".html"

# Convert the input file to HTML and write it to the temporary file
markdown $1 > $MDTEMPFILE

# Open the temporary file with a browser
open /Applications/Google\ Chrome.app $MDTEMPFILE